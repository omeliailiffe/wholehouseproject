var mkdirp = require('mkdirp');

mkdirp('./src/logs').then(err => {
    if (err) console.error(err)
    else console.log('Created logs folder')
});

mkdirp('./src/public/assets').then(err => {
    if (err) console.error(err)
    else console.log('Created assests folder')
});
mkdirp('./src/db').then(err => {
    if (err) console.error(err)
    else console.log('Created DB folder')
});
mkdirp('./src/cert').then(err => {
    if (err) console.error(err)
    else console.log('Created Cert folder')
});
mkdirp('./src/tmp_uploads').then(err => {
    if (err) console.error(err)
    else console.log('Created Temp Upload folder')
});
