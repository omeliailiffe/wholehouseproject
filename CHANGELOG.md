#CHANGELOG
V1.3.0-beta.4:
    Notes:
      - This small update improves a range of database and image features. Images are compressed upon upload and thumbnails are generated to improve the loading of the homepage
    Added:
      - Added Temporary code to migrate datebase images to new system
      - Added thumbnail generation and image commpression on upload using sharp
    Changed:
      - Images are now stored using a better naming scheme
      - Updated image uploading script to be more reliable
    Fixed:
      - Fixed config errors but require config module on index.js, add.js & view.js
    Known Issues:
      - Images uploaded are left in the tmp_uploads directory. These need to be removed.
V1.3.0-beta.3:
    Notes:
      - This small update gives the ability to disable https if you are using a reverse proxy
    Added:
      - Added https.enabled to config
      - if statment switches between http and https
    Known Issues:
      - Expired sessions are not being removed
V1.3.0-beta.2:
    Notes:
      - Moved the session manager to session.js and changed user databases to be stored in seperate files
      - I did this to add the ability to encrypt each one but i'm unsure how to do this effectively.
      - The server now runs on https only
    Added:
      - Session.js - to manage the session cookies
      - Added database file encryption
      - Added certificate folder for https certificates
      - Added express-enforces-ssl to redirect http traffic to https
      - Added port to config
      - Added crytpo.enabled to config. Setting to true will encrypt userdatabase files with crypto.globalkey
      - User databases are now stored in seperate files
      - User databases are stored in yaml files
    Removed:
      - Removed http support
      - Config.js has been replace by node-config
      - Removed console logging of flash messages
    Known Issues:
      - Expired sessions are not being removed
      - Root address is not being redirected to https (https://github.com/hengkiardo/express-enforces-ssl/issues/12)
V1.3.0-beta.1:
    Notes:
      - Config is now at src/config/ and is being managed by node-config
    Added:
      - Node-config module for handling default and local config
    Changed:
      - Change cookie.secure to false - It was breaking everything
      - Move parseCredentials to util.js
    Removed:
      - Config.js has been replace by node-config
      - Removed console logging of flash messages
    Fixed:
      - Fixed a number of small mistakes in the changelog
    Known Issues:
      - Expired sessions are not being removed
V1.3.0-beta.0:
    Notes:
      - Majoring changed how users are authenticated
      - This will pave the way for https support, encryption of the database, and user configuration.
      - It also makes this project largly in accessible as you need an openlpad server, I would like to create the configuration for passport-local and have it as an option
      - I've added a config.js and util.js to keep the code easy to understand
    Added:
      - Passport library authentication
      - Passport-ldapauth
      - util.js - for various helpful utilities
      - config.js - for configuration functions and settings
      - added support for flash messages to be sent to webviews
    Changed:
      - The current users name and details can now be acessed via util.findUser
      - Cookies now persist for 1 hour
    Deprecated:
    Removed:
    Fixed:
      - Fixed creating objects without images
      - Fixed shortid characters
    Security:
      - All user authentication is now handled by an openlpad server
    Known Issues:
      - Expired sessions are not being removed
V1.2.0:
    Notes:
      - A bunch of boring small fixes.
      - Now with client side compression uploading images should be a lot faster, and objects will take less space on the server.
    Added:
      - Restructured repo and added install scripts
      - Added image blobs
      - Compressorjs to compress images before uploading to server
      - Prevented files other than images from being uploaded.
    Changed:
      - Changed ID chars to work with barcodes.
    Deprecated:
    Removed:
    Fixed:
      - Temporary files now get removed. Not sure why this wasn't working but it does know so...
      - Prevented duplicate id being added to database after updating objects.
      - Fixed file uploads from android.
    Security:
V1.1.0:
    Notes:
      - Improved a number of add form issues, and created a changelog system with popup
      - Also altered npm start to use forever.js
    Added:
      - added polling after pressing submit
      - added update popup and changelog
      - added a changelog
      - implemented changelog popup on login
    Changed:
      - npm start now uses forever.js
    Deprecated:
    Removed:
    Fixed:
      - fixed date entry on safari
      - fixed form clearing on "submit and add new"
    Security:
V1.0.0:
  Notes:
    - Inital Release
  Added:
  Changed:
  Deprecated:
  Removed:
  Fixed:
  Security:
