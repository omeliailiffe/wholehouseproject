const path = require('path')
const fs = require('fs')

const express = require('express');
const router = express.Router();

//DATABASE STUFF
const db = require('../session');
const getUserdb = require('../db');
const util = require('../util');
const config = require('config');
//=========Routes=========//

// /api/v1/

router.get('/item/:itemID', function (req, res) {
  var item;
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  if(req.params.itemID){
    item = userdb.get('items')
             .getById(req.params.itemID)
             .value();
    photos = userdb.get('items').getById(req.params.itemID).get("assets").filter({type: "image"}).value();
  }
  res.send({
    item: item,
    user: user,
    photos: req.params.itemID ? userdb.get('items').getById(req.params.itemID).get("assets").filter({type: "image"}).value() : "",
    invalidID: req.params.itemID && !item
  });
});

router.post('/item/(:itemID)?', function (req, res) {
  const data = req.fields
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  const itemID = req.params.itemID;
  //process tags
  //parse tagString
  if(data.tags){
    tagArray = data.tags.split(",");
    data.tags = tagArray;
  }
  if(data.materials){
    materialsArray = data.materials.split(",");
    data.materials = materialsArray;
  }

  //add data to db(user)
  if(itemID){
    item = userdb.get("items")
             .getById(itemID)
             .assign(data)
             .write();
  } else {
    item = userdb.get('items')
             .insert(data)
             .write();
  }
  //====== proccess files ======
  const photos = req.files.compressedPhotos
  //are there files to process?
  if(photos){
    //make the "assests" db(user)
    if(!userdb.get("items").getById(item.id).get("assets").value()){
      assetsDB = userdb.get("items")
                  .getById(item.id)
                  .assign({assets:[]})
                  .get("assets");
      assetsDB.write();
    } else {
      assetsDB = userdb.get("items")
                  .getById(item.id)
                  .get("assets");
    }

    //move files
    if(Array.isArray(photos)){
      for(var i = 0; i<photos.length;i++){
        util.uploadPhoto(photos[i], assetsDB,  item.id, user);
      }
    } else if (photos.type != "application/octet-stream"){
      util.uploadPhoto(photos, assetsDB, item.id, user);
    }
  }
  console.log(user + ": Data Received. Added Item: " + item.id);
  res.send({"item":item,"tags":userdb.get("items").flatMap("tags").uniq().value()});

});

//=====DELETE IMAGE=====//
router.delete("/:itemID/:imageID", function (req, res) {
  const data = req.fields;
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  imageDB = userdb.get('items')
              .getById(req.params.itemID)
              .get("assets");
  image = imageDB.getById(req.params.imageID).value();

  console.log(user + ": Removing Image: " + image.id);
  fs.unlink(path.join("db", user,image.path), function(err){
    if(err){
      console.log(err);
      res.send("Error");
    } else {
      fs.unlink(path.join("db",user,image.thumb), function(err){
        if(err){
          console.log(err);
          res.send("Error");
        } else {
          imageDB.remove({id:image.id}).write();
          res.send("Success");
        }
      });
    }
  });
});

router.get('/image/:itemID/:imageID', function (req, res) {
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  imageDB = userdb.get('items')
              .getById(req.params.itemID)
              .get("assets");
  image = imageDB.getById(req.params.imageID).value();
  if(image){
    res.sendFile(image.path, {
      root: path.join(appRoot, "db", user)
    });
  } else {
    res.status(404).send("This file does not exist");
  }
})

router.get('/thumb/:itemID/:imageID', function (req, res) {
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  imageDB = userdb.get('items')
              .getById(req.params.itemID)
              .get("assets");
  image = imageDB.getById(req.params.imageID).value();
  if(image){
    res.sendFile(image.thumb, {
      root: path.join(appRoot, "db", user)
    });
  } else {
    res.status(404).send("This file does not exist");
  }
})

router.get('/lists', function (req, res) {
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  res.send({
    user: user,
    tagList: userdb.get("items").flatMap("tags").uniq(),
    materialList: userdb.get("items").flatMap("materials").uniq(),
    collectionList: userdb.get("items").flatMap("collection").uniq(),
  });
});

module.exports = router;
