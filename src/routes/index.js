const express = require('express');
const router = express.Router();
const passport = require('passport');
const config = require('config');

//DATABASE STUFF
const db = require('../session');
const getUserdb = require('../db');
const util = require('../util');

//CHECK LOGIN
function loginRequired(req, res, next) {
    var sess = req.session;
    const user = util.findUser(sess);

    if (!user) {
      console.log("user not found")
      return res.status(401).redirect("/login");
    }
    next();
}

router.get("/login", function (req, res) {
  const flashMessage = req.flash('error')[0];
    res.render("login",{
      flash: flashMessage,
    });
});

router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/login");
});

router.post('/login',
  passport.authenticate('local', {failureRedirect: '/login',
                                     failureFlash: true }),
  function (req, res) {
      const sess = req.session;
      const user = util.findUser(sess);
      console.log("New login: " + user);
      res.redirect("/")
  }
);

//ROUTE THINGS
router.get('/', loginRequired, function (req, res) {
    var sess = req.session;
    const user = util.findUser(sess);
    const userdb = getUserdb(user, config.crypto.globalKey);
    var items = userdb.get('items'); // Find all items in the collection

    var changelogFiltered = util.changelog.pickBy((value, key)=>{return key!="Unreleased"});
    var showChangelog = function (){
      if (userdb.get("version").value() != changelogFiltered.keys().value()[0]){
        userdb.assign({version: changelogFiltered.keys().value()[0]}).write();
        return true;
      } else {
        return false;
      }
    }
    res.render('home', {
      title: "Whole House Project",
      user: user,
      items: items,
      navSearch: true,
      tagList: userdb.get("items").flatMap("tags").uniq().value(),
      collectionList: userdb.get("items").flatMap("collection").uniq().value(),
      changelog: changelogFiltered.value(),
      showChangelog: showChangelog
    });
});

router.use("/view", loginRequired, require("./view"));

router.use("/add", loginRequired, require("./add"));

router.use("/api/v1/", loginRequired, require("./api"));


module.exports = router;
