const path = require('path')
const fs = require('fs')

const express = require('express');
const router = express.Router();

//DATABASE STUFF
const db = require('../session');
const getUserdb = require('../db');
const util = require('../util');
const config = require('config');

//=========Routes=========//

router.get('/(:itemID)?', function (req, res) {
  var item, photos;
  const sess = req.session;
  const user = util.findUser(sess);
  const userdb = getUserdb(user, config.crypto.globalKey)
  const itemID = req.params.itemID
  if(itemID){
    item = userdb.get('items')
             .getById(itemID)
             .value();
    photos = userdb.get('items').getById(itemID).get("assets").filter({type: "image"}).value();
  }
  res.render('add', {
    item: item,
    user: user,
    photos: photos,
    submitAlert: item ?  "Object Successfully Updated" : "New Object Successfully Submitted",
  });
});

module.exports = router;
