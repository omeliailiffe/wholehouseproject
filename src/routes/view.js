const express = require('express');
const router = express.Router();

//DATABASE STUFF
const db = require('../session');
const getUserdb = require('../db');
const util = require('../util')
const config = require('config');
//=========Routes=========//

router.get("/object/:itemID", function(req, res) {
    const sess = req.session;
    const user = util.findUser(sess);
    const userdb = getUserdb(user, config.crypto.globalKey);
    const itemID = req.params.itemID;
    item = userdb.get('items')
           .getById(itemID)
           .value();
    console.log(user + ": Serving "+item.id+ " to be veiwed.")
    res.render('view', {
        title: "Whole House Project",
        item: item,
	user: user,
        photos: userdb.get('items').getById(itemID).get("assets").filter({type: "image"}).value()
    });
});


module.exports = router;
