var collectionFilter = [],
    tagFilter = [];

$("#collectionSelect").find("input").click(function() {
    if(this.checked) {
      collectionFilter.push(this.name);
      $(this).closest('label').addClass('btn-primary').removeClass('btn-outline-primary');
    } else {
      _.pull(collectionFilter, this.name);
      $(this).closest('label').addClass('btn-outline-primary').removeClass('btn-primary');
    }
});

$("#tagSelect").find("input").click(function() {
    if(this.checked) {
      tagFilter.push(this.name);
      $(this).closest('label').addClass('btn-primary').removeClass('btn-outline-primary');
    } else {
      _.pull(tagFilter, this.name);
      $(this).closest('label').addClass('btn-outline-primary').removeClass('btn-primary');
    }
});

$(document).ready( function() {
  if($("#objectCards").children().length == 0)
  $("#noObjects").show().fadeTo(200,1);
});

$.expr[":"].containsNoCase = function(el, i, m) {
    var search = m[3];
    if (!search) return false;
    return eval("/" + search + "/i").test($(el).text());
};

$('#searchbox-input').keyup( function () {
    $('.card').parent().show();
    var filter = $(this).val(); // get the value of the input, which we filter on
    if(filter!==""){
      $('#objectCards').find(".card-title:not(:containsNoCase(" + filter + "))").closest(".card-container").hide();
    }

});

$("#collectionSelect, #tagSelect").find("input").change(function(){
  updateConditionalDisplay();
  let objectsShowing = $(".card").filter(":visible").length
  if (objectsShowing !=$(".card").length){
    $("#objectsShown").text("Showing " + objectsShowing + " object" + (objectsShowing===1 ? "." : "s."));
    $("#objectsShown").show();
  } else {
    $("#objectsShown").hide();
  }
});


function updateConditionalDisplay(){
  $(".card").parent().hide();
  $(".card").filter(function(){return attrArrayFilter($(this),"tags", tagFilter)})
            .filter(function(){return attrArrayFilter($(this),"collections", collectionFilter)})
            .parent().show();
}

function attrArrayFilter(obj, attr, array){
  let bool=0;
  if (typeof($(obj).attr(attr))!='undefined'){
    attrArray = $(obj).attr(attr).split(',');
    _.forEach(attrArray, function(value){
      if( _.includes(array,value)){
        bool+=1;
      };
    });
    return bool === array.length;
  } else {
    return 0;
  }
}
