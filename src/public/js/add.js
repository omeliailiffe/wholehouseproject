if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };
};

var pageName = window.location.pathname.split("/").last();
if(pageName != "add"){
  var itemID = pageName;
} else {
  itemID = '';
}

$(document).ready( function() {
  if(itemID){
    $.ajax({
      url: "/api/v1/item/" + itemID,
      success: function (data) {
        console.log(data);
        //display message if invalid id
        if(data.invalidID){
          window.history.replaceState(null,null,"/add");
          $("#invalidIDAlert").show().fadeTo(200,1);
          window.setTimeout(function() {
              $("#invalidIDAlert").fadeTo(200, 0, function(){
                  $(this).hide();
              });
          }, 4000);
        }

        //populated form with data
        $(document).ready( function() {
          var selects = $("select");
          var itemDefault = {"acquisition-method":"Purchased New", "condition": "Excellent"};
          var item = data.item;
          for(let select of selects){
            select.value = item[select.name];
          }
        });

        var existingTags = data.item.tags
        for(var i=0;i<existingTags.length;i++){
          $('#tags').tagsinput("add",existingTags[i]);
        }

        var existingMaterials = data.item.materials;
        for(var i=0;i<existingMaterials.length;i++){
          $('#materials').tagsinput("add",existingMaterials[i]);
        }
      },
      error: function (res) {
        console.log(res);
      }
    });
  }
  $.ajax({
    url: "/api/v1/lists",
    success: function(data){
      //=======COLLECTION========
      var collectionList = new Bloodhound({
        local: data.collectionList,
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
      });

      collectionList.initialize();

      $('#collection').typeahead({},
      {
        name: "collectionList",
        source: collectionList
      });

      //=======TAGS========
      var tagList = new Bloodhound({
        local: data.tagList,
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
      });

      tagList.initialize();

      $('#tags').tagsinput({
        typeaheadjs: {
          name: "Tags",
          source: tagList.ttAdapter()
        }
      });

      //=======MATERIALITY========
      var materialList = new Bloodhound({
        local: data.materialList,
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
      });

      materialList.initialize();

      $('#materials').tagsinput({
        typeaheadjs: {
          name: "Materiality",
          source: materialList.ttAdapter()
        }
      });
    }
  });
});

//updates slider number readouts
$(document).on("input", "#value", function(e){
  $("#value-output").val($(this).val());
});
$(document).on("change", "#value-output", function(e){
  $("#value").val($(this).val());
});

//POPup on imageclick
$('#imageVeiwer').on('show.bs.modal', function (event) {
  var image = $(event.relatedTarget) // Button that triggered the modal
  var modal = $(this)
  modal.find('.modal-body img').attr("src", image.attr('src'));
  modal.find('.modal-body img').attr("id", image.attr('id'));
})

//REMOVES IMAGES VIA MODAL
function removeImage(){
  var image = $('#imageThumbs').find('[src="'+$("#imageVeiwer").find("img").attr("src")+'"]');

  if(image.attr("data-uploaded")=="true"){
    console.log("Image is uploaded. Deleting from server.")
    // var itemID = new URLSearchParams(window.location.search).get('itemID');
    var imageID = image.attr("id");
    // var data = {"imageID":imageID, "itemID":itemID};
    $.ajax({
      url: '/api/v1/'+itemID+"/"+imageID,
      type: 'DELETE',
      success: function (res) {
        if(res=='Success'){
          $("#imageVeiwer").modal('hide');
          image.parent().remove();
        } else {
          $("#imageVeiwer").modal('hide');
        }
      }
    })
  } else {
    $("#imageVeiwer").modal('hide');
    image.parent().remove();
  }
  if ($('#imageThumbs').find("img").length == 0){
    $("#placeholder").show();
  }
}


var fileList = [];
//CALLED WHEN FILE ADDED
$(document).on('change', ':file', function() {
  fileList = [];
    var files = $(this).get(0).files;
    var filePreviews = $('#imageThumbs');
    //delete old files
    filePreviews.find('[data-uploaded="false"]').parent().remove();
    //add new files
    $("#placeholder").hide();
    for(let file of files){
      new Compressor(file, {
        quality: 0.9,
        maxWidth: 1200,
        maxHeight: 1200,
        success(result){
          let src = URL.createObjectURL(result);
          fileList.push({"result":result,"src":src,"name":file.name});
          filePreviews.append('<div class="p-2 col-6 col-sm-4 col-md-3 col-lg-2"><img src="'+src+'" class="img-thumbnail border-success" data-toggle="modal" data-target="#imageVeiwer" data-uploaded=false></div>');
        },
        error(err) {
          console.log(err.message);
        }
      });
    }

});

//CALLED WHEN FORM IS SUBMITTED
$(document).on("click", "#submitForm", function(){
  submitForm();
});


function submitForm () {
    // var itemID = new URLSearchParams(window.location.search).get('itemID');
    var form = document.getElementById('form');
    var formData = new FormData(form);
    formData.delete("photos");
    for(let file of fileList){
      formData.append("compressedPhotos",file.result, file.name);
    }
    // if(itemID){formData.append("id", itemID);}
    if (!form.checkValidity()){
      $("#invalidForm").show().fadeTo(200,1);
      form.classList.add('was-validated');
      window.setTimeout(function() {
          $("#invalidIDAlert").fadeTo(200, 0, function(){
              $(this).hide();
          });
      }, 4000);
    } else {
      $("#submitModal").modal('show');
      $.ajax({
          url: '/api/v1/item/' + itemID,
          type: 'POST',
          data: formData,
          enctype: 'multipart/form-data',
          success: function (res) {
              $("#submitModal-footSuccess").show(); //showbuttons
              $("#submitModal-spinner").hide();
              $("#submitModal-text").html("Submitted");

              console.log("Form Submited");

              $(document).on("click", "#submitModal-formreset", function(e){
                $(document).off("click", "#submitModal-formreset");
                $(document).off("click", "#submitModal-moveon");
                window.location.href = "/add"
              });

              $(document).on("click", "#submitModal-moveon", function(e){
                $(document).off("click", "#submitModal-formreset");
                $(document).off("click", "#submitModal-moveon");
                window.history.replaceState(null,null,"/add/"+res.item.id);
                window.location.href = "/view/object/"+res.item.id;
              });
          },
          error: function (res) {
            $("#submitModal-footError").show(); //showbuttons
            $("#submitModal-spinner").hide();
            $("#submitModal-text").html("Error Submitting");

          },
          cache: false,
          contentType: false,
          processData: false
      });
    }
  };
