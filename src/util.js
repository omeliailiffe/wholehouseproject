const path = require('path')
const fs = require('fs')
const sharp = require('sharp')
//=====================Changelog=====================
const yaml = require('js-yaml');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('../CHANGELOG.md', {
  defaultValue: [],
  serialize: (array) => yaml.dump(array),
  deserialize: (string) => yaml.safeLoad(string)
})

const changelog = low(adapter);

function findUser(sess) {
  if(sess.passport){
    return sess.passport.user;
  } else {
    return null;
  }
}

function Credentials (name, pass) {
  this.name = name
  this.pass = pass
}

function parseCredentials(req){
  return new Credentials(req.fields.username, req.fields.password);
}

function uploadPhoto(asset, assetsDB, itemID, userID, callback){
  //make directory
  const folder = path.join('db',userID, item.id)
  console.log(folder);
  if(!fs.existsSync(folder)){
    fs.mkdirSync(folder);
  }
  image = assetsDB.insert({type:"image"}).write();

  assetName = image.id + ".jpg"
  thumbName = image.id + "-thumb.jpg"
  newPath = path.join(item.id, assetName);
  thumbPath = path.join(item.id, thumbName);

  assetsDB.getById(image.id)
    .assign({
      path:newPath,
      thumb:thumbPath,
      src: path.join(itemID, image.id)
    })
    .write();

  sharp(asset.path)
  // .resize({
  //   width: 1200,
  //   height: 1200,
  //   fit: 'inside'
  // })
  // .jpeg({
  //   quality: 80,
  //   force: true
  // })
  .toFile(path.join(folder,assetName), function(err) {
    if(err){console.error(err)}
  })

  sharp(asset.path)
  .resize({
    width: 600,
    height: 400,
    fit: 'inside'
  })
  .jpeg({
    quality: 80,
    force: true
  })
  .toFile(path.join(folder,thumbName), function(err) {
    if(err){console.error(err)}
  });
}

async function processImages(items){
  numItems = items.value().length
  for(var i=0;i<numItems;i++){
    item = await items.nth(i);
    assetsDB = await item.get('assets');
    if(assetsDB.value()){
      for(var x=0;x<assetsDB.value().length;x++){
        var asset = await assetsDB.nth(x);
        if(!asset.get("thumb") && asset.get('type')=='image'){
          await console.log("updating image "+ item.get('id'))
          await util.updateThumb(asset, item.value());
        }
      }
    }
    console.log('Processed ',i+1,'/',numItems)
  }
}

async function updateThumb(asset, item){
  folder = path.join( '/assets/' +item.id);
  assetName = asset.get('id') + ".jpg"
  thumbName = asset.get('id') + "-thumb.jpg"
  newPath = path.join('public',folder, assetName);
  thumbPath = path.join('public',folder, thumbName);

  await sharp(path.join('public', asset.get('path').value()))
  .resize({
    width: 1200,
    height: 1200,
    fit: 'inside'
  })
  .jpeg({
    quality: 80,
    force: true
  })
  .toFile(newPath, function(err) {
    if(err){console.error(err)}
    else{console.log("resized image")}
  });

  await sharp(path.join('public', asset.get('path').value()))
  .resize({
    width:600,
    height: 400,
    fit: 'inside'
  })
  .jpeg({
    quality: 80,
    force: true
  })
  .toFile(thumbPath, function(err) {
    if(err){console.error(err)}
    else{console.log("generated thumb")}
  });

  await asset.assign({
      processed: 1,
      path:path.join(folder, assetName),
      thumb:path.join(folder, thumbName)
    })

  if(path.join('public', asset.get('path').value()) != path.join(folder, assetName) && !asset.get('original').value()){
    await asset.assign({
        original:path.join('public', asset.get('path').value())
      })
      .write();
  }
}

exports.findUser = findUser;
exports.changelog = changelog;
exports.parseCredentials = parseCredentials;
exports.uploadPhoto = uploadPhoto;
exports.updateThumb = updateThumb;
exports.processImages = processImages;
