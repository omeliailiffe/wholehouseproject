// app.js
//EXPRESS STUFF
const path = require('path')
const config = require('config');
const fs = require('fs')
const express = require('express')
const exphbs = require('express-handlebars')
const app = express()
const routes = require('./routes/index');
var https = require('https');
var forceSSL = require('express-enforces-ssl');
const formidableMiddleware = require('express-formidable');
const session = require('express-session');
const LowdbStore = require('lowdb-session-store')(session);
var passport = require('passport');
var LocalStrategy = require('passport-local');
var flash = require('connect-flash');
const db = require('./session');
const getUserdb = require('./db');
const util = require('./util.js')

require('console-stamp')(console, 'HH:MM:ss');

global.appRoot = path.resolve(__dirname);

//SETUP EXPRESS
//PASSPORT
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  user = db.get("sessions").find({"user":"omelia"})
  done(null, user);
});

// Object.assign(config.ldap, {credentialsLookup: util.parseCredentials})
passport.use(new LocalStrategy(
  {
    credentialsLookup: util.parseCredentials
  },
  function(username, password, cb) {
    const userdb = getUserdb(username, config.crypto.globalKey);
    if (!userdb) { return cb(null, false,{ message: 'Incorrect Details' }); }
    if (userdb.get("user").get("password").value() != password) { return cb(null, false, { message: 'Incorrect Details' }); }
    return cb(null, username);
  }
));

app.use(session({
  secret: 'oaehgajehliaehfaweoiucnweoprunpoecunrpoaecnrpoeharpoe',
  cookie: {
    maxAge: 3600000
   },
  resave: false,
  saveUninitialized: true,
  store: new LowdbStore(db.get('sessions'), {
    ttl: 86400
  })
}))

app.use(flash())

app.use(passport.initialize());

app.use(passport.session());

//SETUP HANDLEBARS
app.engine('.hbs', exphbs({
  extname: '.hbs',
  helpers: {
    default: function (value, safeValue) {
        var out = value || safeValue;
        return out;
    }
  },
  helpers:require('handlebars-helpers')()

}));

app.set('view engine', '.hbs');

app.use(formidableMiddleware(config.formidable));

app.use(express.static('public'));

app.use('/', routes)

if(config.https.trust_proxy){
  app.enable('trust proxy');
}

if(config.https.enabled){
  app.use(forceSSL());
  https.createServer({
    key: fs.readFileSync(config.https.key),
    cert: fs.readFileSync(config.https.cert)
  }, app)
  .listen(config.port, function () {
    console.log('Whole House Project listening on: ', config.port);
  })
} else {
  app.listen(config.port, function () {
    console.log('Whole House Project listening on: ', config.port);
  })
}
