const path = require('path')
const fs = require('fs')

const express = require('express');
const router = express.Router();

//DATABASE STUFF
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const shortid = require('shortid');
const crypto = require('crypto');
const yaml = require('js-yaml');
const config = require('config');

//Masive thanks to https://github.com/peterjgrainger/crypto-example
function encrypt(value, key){
    const iv = new Buffer(crypto.randomBytes(16));
    const cipher = crypto.createCipheriv(config.crypto.algorithm, key, iv);
    let crypted = cipher.update(value, config.crypto.inputEncoding, config.crypto.outputEncoding);
    crypted += cipher.final(config.crypto.outputEncoding);
    return `${iv.toString('hex')}:${crypted.toString()}`;
}

function decrypt(value, key){
    const textParts = value.split(':');

    //extract the IV from the first half of the value
    const IV = new Buffer(textParts.shift(), config.crypto.outputEncoding);

    //extract the encrypted text without the IV
    const encryptedText = new Buffer(textParts.join(':'), config.crypto.outputEncoding);

    //decipher the string
    const decipher = crypto.createDecipheriv(config.crypto.algorithm, key, IV);
    let decrypted = decipher.update(encryptedText,  config.crypto.outputEncoding, config.crypto.inputEncoding);
    decrypted += decipher.final(config.crypto.inputEncoding);
    return decrypted.toString();
}

function userdb(user, secret) {
  const dbname = 'db/'+user+'.yaml'
  var adapter
  if(config.crypto.enabled){
    adapter = new FileSync(dbname, {
      serialize: (array) => encrypt(yaml.dump(array), secret),
      deserialize: (string) => yaml.safeLoad(decrypt(string), secret)
    });
  } else {
    adapter = new FileSync(dbname, {
      serialize: (array) => yaml.dump(array),
      deserialize: (string) => yaml.safeLoad(string)
    });
  }

  const _userdb = low(adapter);

  _userdb._.mixin(require('lodash-id'));
  _userdb._.mixin({
      createId: function(collectionName, doc) {
          return shortid.generate();
      }
});

  _userdb.defaults({"user": {user: user, password: "" }, "items": [], "version":"Initial"}).write();
  return _userdb
}

module.exports = userdb
