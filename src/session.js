const path = require('path')
const fs = require('fs')

const express = require('express');
const router = express.Router();

//DATABASE STUFF
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('db/sessions.json');
const shortid = require('shortid');
const db = low(adapter);

db._.mixin(require('lodash-id'));
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-');
db._.mixin({
    createId: function(collectionName, doc) {
        return shortid.generate();
    }
});

db.defaults({sessions:[]}).write();

module.exports = db;
